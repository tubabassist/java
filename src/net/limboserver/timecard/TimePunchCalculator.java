package net.limboserver.timecard;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class TimePunchCalculator {
	public static void main(String[] args) throws ParseException {
		final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("h:mma");
		Scanner input = new Scanner(System.in);
		Calendar in = Calendar.getInstance();
		Calendar lunchOut = Calendar.getInstance();
		Calendar lunchIn = Calendar.getInstance();
		Calendar out = Calendar.getInstance();

		System.out.print("IN = ");
		in.setTime(DATE_FORMAT.parse(input.nextLine().replaceAll("\\s", "")));
		Calendar lunchOutBy = (Calendar) in.clone();
		lunchOutBy.add(Calendar.HOUR, 6);
		System.out.println("Lunch OUT by = "
				+ DATE_FORMAT.format(lunchOutBy.getTime()));

		System.out.print("Lunch OUT = ");
		lunchOut.setTime(DATE_FORMAT.parse(input.nextLine().replaceAll("\\s",
				"")));
		Calendar lunchInBy = (Calendar) lunchOut.clone();
		lunchInBy.add(Calendar.MINUTE, 30);
		System.out.println("Lunch IN after = "
				+ DATE_FORMAT.format(lunchInBy.getTime()));

		System.out.print("Lunch IN = ");
		lunchIn.setTime(DATE_FORMAT.parse(input.nextLine()
				.replaceAll("\\s", "")));

		Calendar outBy = (Calendar) in.clone();
		outBy.add(Calendar.HOUR, 8);
		outBy.setTime(new Date(outBy.getTimeInMillis()
				+ (lunchIn.getTimeInMillis() - lunchOut.getTimeInMillis())));
		System.out.println("OUT by = " + DATE_FORMAT.format(outBy.getTime()));

		System.out.print("OUT = ");
		out.setTime(DATE_FORMAT.parse(input.nextLine().replaceAll("\\s", "")));
		input.close();
	}
}